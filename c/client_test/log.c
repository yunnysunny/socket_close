/*
* File: log.c
* Desc: 
* Copyright (c) SWXA 2009
*
* Date        By who     Desc
* ----------  ---------  -------------------------------
* 2009.04.20  Yaahao     Created
* 2010/11/11  GAO        Optimized
*/

#include <stdio.h>
#include <time.h>
#include <string.h>
#include <stdarg.h>
#include <ctype.h>
#include <stdlib.h>

#if defined(WIN32) || defined(WIN64)
#include <io.h>
#include <stdlib.h> 
#include <Windows.h>
#else
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/types.h>
#include <pthread.h>
#endif

#include "log.h"

//#define PRINT_LOG 1 //Print to screen

/*########################################################*/
/*########################################################*/
/*###  重要说明：其他模块使用该日志时，建议修改  ###*/
/*###  宏定义的模块名称和全局变量名              ###*/
/*########################################################*/
/*########################################################*/

char         G_swsds_log_file[512] = "";       //Global
unsigned int G_swsds_log_level     = LOG_ERROR;//Global
unsigned int G_swsds_log_max_size  = 0;        //Global

/*########################################################*/
/*########################################################*/
/*###  重要说明：其他模块使用该日志时，建议修改  ###*/
/*###  宏定义的模块名称和全局变量名              ###*/
/*########################################################*/
/*########################################################*/

/*Windows: 在系统安装盘符根目录*/
/*Linux/Unix: 在TMP目录*/

static FILE * OpenLogFile(char *sPath, char *sLogFile, char* sModule)
{
	FILE *fp;
	struct tm *newtime;
	time_t aclock;
	char sRealLogFile[300];
	char sLogPath[256];

	/*Get current time*/
	time( &aclock );                 
	newtime = localtime( &aclock ); 

	/*Get log file name*/
	if(strlen(sLogFile) == 0)
	{
	#if defined(WIN32) || defined(WIN64)
		GetWindowsDirectoryA(sLogPath, sizeof(sLogPath)-1);
		sLogPath[2] = '\0'; /*只取系统盘符*/
		strcat(sLogPath, "\\");
		strcat(sLogPath, sPath);
		strcat(sLogPath, "\\");
	#else
		sprintf(sLogPath, "/tmp/%s/", sPath);
	#endif
		sprintf(sRealLogFile,"%s%s_%4d%02d%02d.log",sLogPath, sModule, newtime->tm_year+1900,newtime->tm_mon+1,newtime->tm_mday);
		/*Open log file*/
		fp = fopen(sRealLogFile,"a+");   
	}
	else
	{
		/*Open log file*/
		 fp = fopen(sLogFile,"a+");   
	}

	return fp;
}

void LogMessage(char *sPath, char *sLogFile, char* sModule, int nLogLevel,
	char *sFile,int nLine,unsigned int unErrCode,const char *sMessage)
{
#if defined(WIN32) || defined(WIN64)
	DWORD nThreadID;
#else
	unsigned int nThreadID;
#endif
	struct tm *newtime;
	time_t aclock;

	/*Open log file*/
	FILE *fp = OpenLogFile(sPath, sLogFile, sModule);
#ifndef PRINT_LOG
	if(NULL == fp)
		return;
#endif
	/*Get current time*/
  	time( &aclock );                 
	newtime = localtime( &aclock ); 
	
	/*Get current threadid*/
#if defined(WIN32) || defined(WIN64)
	nThreadID = GetCurrentProcessId();
	nThreadID = (nThreadID << 16) + GetCurrentThreadId();
#else
	nThreadID = getpid();
	nThreadID = (nThreadID << 16) + (unsigned long int)(pthread_self());
#endif

	/*Write log message*/
	switch(nLogLevel)
	{
	case LOG_CRIT:
		if(NULL != fp)
			fprintf(fp,"\n<%4d-%02d-%02d %02d:%02d:%02d><%s><%ud><Erit>[0x%08x]%s(%s:%d)",newtime->tm_year+1900,newtime->tm_mon+1,newtime->tm_mday,newtime->tm_hour,newtime->tm_min,newtime->tm_sec,sModule,nThreadID, unErrCode, sMessage, sFile,nLine);
#ifdef PRINT_LOG
		printf("\n<%4d-%02d-%02d %02d:%02d:%02d><%s><%ud><Error>[0x%08x]%s(%s:%d)",newtime->tm_year+1900,newtime->tm_mon+1,newtime->tm_mday,newtime->tm_hour,newtime->tm_min,newtime->tm_sec,sModule,nThreadID,unErrCode, sMessage, sFile,nLine);
#endif
		break;
	case LOG_ERROR:
		if(NULL != fp)
			fprintf(fp,"\n<%4d-%02d-%02d %02d:%02d:%02d><%s><%ud><Error>[0x%08x]%s(%s:%d)",newtime->tm_year+1900,newtime->tm_mon+1,newtime->tm_mday,newtime->tm_hour,newtime->tm_min,newtime->tm_sec,sModule,nThreadID, unErrCode, sMessage, sFile,nLine);
#ifdef PRINT_LOG
		printf("\n<%4d-%02d-%02d %02d:%02d:%02d><%s><%ud><Error>[0x%08x]%s(%s:%d)",newtime->tm_year+1900,newtime->tm_mon+1,newtime->tm_mday,newtime->tm_hour,newtime->tm_min,newtime->tm_sec,sModule,nThreadID,unErrCode, sMessage, sFile,nLine);
#endif
		break;
	case LOG_WARNING:
		if(NULL != fp)
			fprintf(fp,"\n<%4d-%02d-%02d %02d:%02d:%02d><%s><%ud><Warning>%s<%d>(%s:%d)",newtime->tm_year+1900,newtime->tm_mon+1,newtime->tm_mday,newtime->tm_hour,newtime->tm_min,newtime->tm_sec,sModule, nThreadID, sMessage, unErrCode, sFile,nLine);
#ifdef PRINT_LOG
		printf("\n<%4d-%02d-%02d %02d:%02d:%02d><%s><%ud><Warning>%s<%d>(%s:%d)",newtime->tm_year+1900,newtime->tm_mon+1,newtime->tm_mday,newtime->tm_hour,newtime->tm_min,newtime->tm_sec,sModule,nThreadID, sMessage, unErrCode, sFile,nLine);
#endif
		break;
	case LOG_INFO:
		if(NULL != fp)
			fprintf(fp,"\n<%4d-%02d-%02d %02d:%02d:%02d><%s><%ud><Info>%s(%d)(%s:%d)",newtime->tm_year+1900,newtime->tm_mon+1,newtime->tm_mday,newtime->tm_hour,newtime->tm_min,newtime->tm_sec,sModule, nThreadID, sMessage,  unErrCode, sFile,nLine);
#ifdef PRINT_LOG
		printf("\n<%4d-%02d-%02d %02d:%02d:%02d><%s><%ud><Info>%s(%d)(%s:%d)",newtime->tm_year+1900,newtime->tm_mon+1,newtime->tm_mday,newtime->tm_hour,newtime->tm_min,newtime->tm_sec,sModule,nThreadID, sMessage,  unErrCode, sFile,nLine);
#endif
		break;
	case LOG_DEBUG:
		if(NULL != fp)
			fprintf(fp,"\n<%4d-%02d-%02d %02d:%02d:%02d><%s><%ud><Debug>%s(%d)(%s:%d)",newtime->tm_year+1900,newtime->tm_mon+1,newtime->tm_mday,newtime->tm_hour,newtime->tm_min,newtime->tm_sec,sModule, nThreadID, sMessage,  unErrCode, sFile,nLine);
#ifdef PRINT_LOG
		printf("\n<%4d-%02d-%02d %02d:%02d:%02d><%s><%ud><Info>%s(%d)(%s:%d)",newtime->tm_year+1900,newtime->tm_mon+1,newtime->tm_mday,newtime->tm_hour,newtime->tm_min,newtime->tm_sec,sModule,nThreadID, sMessage,  unErrCode, sFile,nLine);
#endif
		break;
	case LOG_TRACE:
		if(NULL != fp)
			fprintf(fp,"\n<%4d-%02d-%02d %02d:%02d:%02d><%s><%ud><Trace>%s(%s:%d)",newtime->tm_year+1900,newtime->tm_mon+1,newtime->tm_mday,newtime->tm_hour,newtime->tm_min,newtime->tm_sec,sModule, nThreadID, sMessage, sFile,nLine);
#ifdef PRINT_LOG
		printf("\n<%4d-%02d-%02d %02d:%02d:%02d><%s><%ud><Trace>%s(%s:%d)",newtime->tm_year+1900,newtime->tm_mon+1,newtime->tm_mday,newtime->tm_hour,newtime->tm_min,newtime->tm_sec,sModule,nThreadID, sMessage, sFile,nLine);
#endif
		break;
	default:
		break;
	}
    
	/*Close file handle*/
	if(NULL != fp)
		fclose(fp);
}

void LogData(char *sPath, char *sLogFile, char* sModule, int nLogLevel,
	char *sFile, int nLine, char *sMessage, unsigned char *pBuffer, unsigned int nLength)
{
	int i,j;
	char sLine[128];
	int rowCount = 16;
	char *ch;
	unsigned char low, high;

#if defined(WIN32) || defined(WIN64)
	DWORD nThreadID;
#else
	unsigned int nThreadID;
#endif
	struct tm *newtime;
	time_t aclock;

	/*Open log file*/
	FILE *fp = OpenLogFile(sPath, sLogFile, sModule);
#ifndef PRINT_LOG
	if(NULL == fp)
		return;
#endif

	time( &aclock );
	newtime = localtime( &aclock ); 
	
#if defined(WIN32) || defined(WIN64)
	nThreadID = GetCurrentProcessId();
	nThreadID = (nThreadID << 16) + GetCurrentThreadId();
#else
	nThreadID = getpid();
	nThreadID = (nThreadID << 16) + (unsigned long int)(pthread_self());
#endif

	/*Write log message*/
	if(NULL != fp)
		fprintf(fp,"\n<%4d-%02d-%02d %02d:%02d:%02d><%s><%ud><Debug>%s(%d)(%s:%d)",newtime->tm_year+1900,newtime->tm_mon+1,newtime->tm_mday,newtime->tm_hour,newtime->tm_min,newtime->tm_sec,sModule, nThreadID, sMessage,  nLength, sFile,nLine);
#ifdef PRINT_LOG
	printf("\n<%4d-%02d-%02d %02d:%02d:%02d><%s><%ud><Info>%s(%d)(%s:%d)",newtime->tm_year+1900,newtime->tm_mon+1,newtime->tm_mday,newtime->tm_hour,newtime->tm_min,newtime->tm_sec,sModule,nThreadID, sMessage,  nLength, sFile,nLine);
#endif

	if(nLength > 0)
	{
		i = 0;
		for(i=0;i<nLength/rowCount;i++)
		{
			sprintf(sLine, "0x%08x  ",i*rowCount);
			ch = &sLine[12];
			for(j=0;j<rowCount;j++)
			{
				low = pBuffer[i * rowCount + j] & 0x0f;
				high = pBuffer[i * rowCount + j]>>4;

				if(high <= 9)
					*(ch++) = high + '0';
				else
					*(ch++) = high - 10 + 'A';

				if(low <= 9)
					*(ch++) = low + '0';
				else
					*(ch++) = low - 10 + 'A';

				*(ch++) = ' ';
			}
			*(ch++) = ' ';
			for(j=0;j<rowCount;j++)
			{
				if((pBuffer[i * rowCount + j] >= 33) && (pBuffer[i * rowCount + j] <= 126)) //Visible character
					*(ch++) = pBuffer[i * rowCount + j];
				else
					*(ch++) = '.';
			}
#if defined(WIN32) || defined(WIN64)
			*(ch++) = '\r';
#endif
			*(ch++) = '\n';
			*(ch++) = '\0';
			if(NULL != fp)
				fputs(sLine, fp);
#ifdef PRINT_LOG
			fputs(sLine, stdout);
#endif
		}

		if (nLength%rowCount)
		{
			sprintf(sLine, "0x%08x  ",i*rowCount);
			ch = &sLine[12];
			for(j=0;j<nLength%rowCount;j++)
			{
				low = pBuffer[i * rowCount + j] & 0x0f;
				high = pBuffer[i * rowCount + j]>>4;

				if(high <= 9)
					*(ch++) = high + '0';
				else
					*(ch++) = high - 10 + 'A';

				if(low <= 9)
					*(ch++) = low + '0';
				else
					*(ch++) = low - 10 + 'A';

				*(ch++) = ' ';
			}
			for(j=0;j<rowCount - nLength%rowCount;j++)
			{
				*(ch++) = ' ';
				*(ch++) = ' ';
				*(ch++) = ' ';
			}
			*(ch++) = ' ';
			for(j=0;j<nLength%rowCount;j++)
			{
				if((pBuffer[i * rowCount + j] >= 33) && (pBuffer[i * rowCount + j] <= 126)) //Visible character
					*(ch++) = pBuffer[i * rowCount + j];
				else
					*(ch++) = '.';
			}
#if defined(WIN32) || defined(WIN64)
			*(ch++) = '\r';
#endif
			*(ch++) = '\n';
			*(ch++) = '\0';
			if(NULL != fp)
				fputs(sLine, fp);
#ifdef PRINT_LOG
			fputs(sLine, stdout);
#endif
		}//end if (nLength%rowCount)
	}//end if(nLength > 0)

	/*Close file handle*/
	if(NULL != fp)
		fclose(fp);
}

