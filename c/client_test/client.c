#include "common_socket.h"
#include "log.h"
#include <stdio.h>

#if defined(WIN32) || defined(WIN64)
#include <io.h>
#include <direct.h>
#include <locale.h>
#else
#include <unistd.h>
#include <sys/types.h>
#endif

#define IP_ADDR_LOCAL_STR		"127.0.0.1"
#define IP_PORT_DEFAULT			9010

int main(int argc, char *argv[ ] , char *envp[ ]) {
	SOCKET client;
	const char *ipAddr = NULL;
	unsigned int ret = 0;
#if defined(WIN32) || defined(WIN64)

	WSADATA WsaData;
	if (( WSAStartup(MAKEWORD(1,1),&WsaData)) != 0)
	{
		LOG(LOG_ERROR,-1, "CheckServer->WSAStartup error");
		return -1;
	}
	setlocale(LC_ALL,"chs");
#endif
	if (argc == 2) {
		ipAddr = argv[1];
	} else {
		ipAddr = IP_ADDR_LOCAL_STR;
	}
	ret = CS_GetConnect(&client,ipAddr,9010);
	if (ret == 0) {
		printf("connected success.");
	}
	CloseSocket(client);
	return 0;
}