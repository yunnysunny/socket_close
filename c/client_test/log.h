/*
* File: log.h
* Desc: 
* Copyright (c) SWXA
*
* Date        By who     Desc
* ----------  ---------  -------------------------------
* 2009.04.20  Yaahao     Created
*/


#ifndef _SW_LOG_H_
#define _SW_LOG_H_ 1

/*本日志级别分为6层，实施部署时（生产环境）建议为2或3*/
#define LOG_NONE      0  //不记录日志
#define LOG_CRIT      1  //致命错误，会导致返回错误或引起程序异常
#define LOG_ERROR     2  //一般错误，内部错误不向上层返回错误，建议解决
#define LOG_WARNING   3  //警告信息，不是期望的结果，不会引起错误，但要用户引起重视
#define LOG_INFO      4  //重要变量，有助于解决问题
#define LOG_DEBUG     5  //调试信息，可打印二进制数据
#define LOG_TRACE     6  //跟踪执行，用于跟踪逻辑判断

/*########################################################*/
/*########################################################*/
/*###  重要说明：其他模块使用该日志时，建议修改  ###*/
/*###  宏定义的模块名称和全局变量名              ###*/
/*########################################################*/
/*########################################################*/

#define DEFAULT_LOG_PATH   "swlogs"
#define DEFAULT_LOG_MODULE "swsvs"

extern char         G_swsds_log_file[512]; //Global
extern unsigned int G_swsds_log_level;     //Global
extern unsigned int G_swsds_log_max_size;  //Global

#define LOG(lvl, rv, msg) \
        do { \
        if ((lvl) <= G_swsds_log_level) {\
        	LogMessage(DEFAULT_LOG_PATH, G_swsds_log_file, DEFAULT_LOG_MODULE, lvl, __FILE__, __LINE__, rv, (const char*)(msg));} \
        } while (0)
#define LOGDATA(msg, buf, len) \
        do { \
        if (LOG_DEBUG <= G_swsds_log_level) {\
        	LogData(DEFAULT_LOG_PATH, G_swsds_log_file, DEFAULT_LOG_MODULE, LOG_DEBUG, __FILE__, __LINE__, msg, buf, len);} \
        } while (0)

/*########################################################*/
/*########################################################*/
/*###  重要说明：其他模块使用该日志时，建议修改  ###*/
/*###  宏定义的模块名称和全局变量名              ###*/
/*########################################################*/
/*########################################################*/

void LogMessage(char *sPath, char *sLogFile, char* sModule, int nLogLevel,
	char *sFile,int nLine,unsigned int unErrCode,const char *sMessage);
void LogData(char *sPath, char *sLogFile, char* sModule, int nLogLevel, char *sFile, int nLine, char *sMessage, unsigned char *pBuffer, unsigned int nLength);

#endif /*#ifndef _SW_LOG_H_*/

