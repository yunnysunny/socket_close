#include <iostream>
#include <windows.h>

#include <stdio.h>
#include <winsock.h>
#pragma comment (lib,"ws2_32")

#define MY_SOCKET_PORT									9010
#define MY_SOCKET_BUFFER_SIZE						4096

#define SOCKET_READE_ERROR							0x2
#define SOCKET_MEM_ERROR								0x1



DWORD WINAPI clientThread(LPVOID lpPArAm)
{
	DWORD Ret = 0;
	SOCKET client = *((SOCKET *)lpPArAm);
	char *getBuffer = (char *)malloc(sizeof(char)*MY_SOCKET_BUFFER_SIZE);
	if (getBuffer == NULL)
	{
		Ret = SOCKET_MEM_ERROR;
		goto clear;
	}
	while(true) {
		memset(getBuffer,0,MY_SOCKET_BUFFER_SIZE);
		Ret = recv(client, getBuffer, MY_SOCKET_BUFFER_SIZE, 0);
		if ( Ret == 0 || Ret == SOCKET_ERROR ) 
		{
			printf("对方socket已经退出,Ret【%d】!\n",Ret);
			Ret = SOCKET_READE_ERROR;//接收服务器端信息失败
			break;
		}
	}

clear:
	if (getBuffer != NULL) {
		free(getBuffer);
		getBuffer = NULL;
	}
	//closesocket(client);
	return Ret;
}

int main() {
	WSADATA wsadata;
	SOCKET server;
	SOCKET client;
	SOCKADDR_IN serveraddr;
	SOCKADDR_IN clientaddr;	

	WORD ver=MAKEWORD(2,2);							//判断winsock版本
	WSAStartup(ver,&wsadata);						//初始SOCKET

	server=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);//创建socket句柄

	serveraddr.sin_family=AF_INET;
	serveraddr.sin_port = htons(MY_SOCKET_PORT);
	serveraddr.sin_addr.S_un.S_addr=htonl(INADDR_ANY);

	bind(server,(SOCKADDR*)&serveraddr,sizeof(serveraddr));//绑定socket到当前地址
	
	listen(server,5);//建立socket监听 
	printf("server listen on %d \n", MY_SOCKET_PORT);

	int len=sizeof(clientaddr);
	while(1) {
		client=accept(server,(sockaddr *)&clientaddr,&len);
		printf("accept one connection[%d]\n",client);

		HANDLE hThread=CreateThread(NULL,0,clientThread,(PVOID)&client,0,NULL);//线程逻辑封装在clientThread中 
		WaitForSingleObject(hThread,INFINITE);
		CloseHandle(hThread);

		Sleep(1000);
	}
	

	WSACleanup();
	return 0;
}