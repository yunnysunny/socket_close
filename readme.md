这是博客[《小议socket挥手》](http://blog.csdn.net/yunnysunny/article/details/18994927 "《小议socket挥手》")的配套代码

其中文件夹c下配套c代码，文件夹java下是java代码。

c文件夹下的`client_test`是socket客户端代码，可以在windows vs2010或者linux下编译通过；`socket_server`是socket的服务器端代码，目前仅仅只能通过vs2010编译。

java文件夹下是一个eclipse工程，里面仅仅包含两个java类，一个处理服务器端、一个处理客户端。

博客文中代码片段出处：
1. 代码片段1.1出自 `c/client_test/client.c`    
2. 代码片段1.2出自`c/client_test/common_socket.h`  
3. 代码片段1.3出自`c/socket_server/server.cpp`  
4. 代码片段2.1、2.2出自`c/client_test/common_socket.c`  
5. 代码片段3.1出自`java/src/Client.java`

对于socket关闭的主动发起者，c代码中发起者为客户端（`c/client_test/client.c`），java代码中发起者为服务器端（`java/src/Server.java`），具体可以参考相应代码。  

查看socket的挥手数据包，推荐使用wireshark进行抓包，注意使用两台机器进行测试，如果没有机器，可以使用虚拟机，这时候虚拟机的网络连接方式需要使用桥接或者host only，以保证客户端和服务器端的IP是不一样的，因为wireshark不能抓取127.0.0.1的回路数据包。抓取的时候可以使用`tcp.port == 9090`这个filter，因为示例代码中使用的tcp监听端口为9090，这样能够仅仅抓取到于代码相关的数据包，防止出现其他无用数据包的干扰。