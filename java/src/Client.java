import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;



public class Client {
	public static void main(String[] argv) throws UnknownHostException {
		Socket client = new Socket();//,9090
		try {
			client.connect(new InetSocketAddress("192.168.56.101",9090));
			
			while(true){	
				
				try {
					int c = client.getInputStream().read();
					if (c > 0) {
						System.out.print((char) c);
					} else {//如果对方socket关闭，read函数返回-1
						//client.getOutputStream().write("hello".getBytes());//这句话将会触发RST
						break;
					}
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					Thread.currentThread().sleep(2000);
//					client.getOutputStream().write("hello".getBytes());
					
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		} catch (IOException e2) {
			e2.printStackTrace();
		} finally {
			try {
				client.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}
}
